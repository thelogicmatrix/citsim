﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    public static DateTime CDT; //CurrentDateTime

    public static TimeSpan MLE; //MaxLifeExpectancy

    public List<_Primitive> Primitives;

    public static List<Entity> TheLiving, TheDeceased;

    public GameObject EntityPrefab;

    public Text TimeSkipInputField;

    void Awake()
    {

        Application.targetFrameRate = -1;

        TheLiving = TheDeceased = new List<Entity>();

        CDT = DateTime.Now;

        MLE = TimeSpan.FromDays(1000 * 365);

        if (Primitives == default)
        {
            print("No Primitives");
        }

        foreach (_Primitive P in Primitives)
        {
            var E = Instantiate(EntityPrefab);

            var EComp = E.GetComponent<Entity>();

            if (EComp == default)
            {
                continue;
            }

            EComp.Age = TimeSpan.FromDays(P.AgeInYears * 365);

            EComp.NetWorth = P.NetWorth;

            EComp.Init();

            TheLiving.Add(EComp);
        }
    }

    void Update()
    {
    }

    public void TimeSkip()
    {
        if (!float.TryParse(TimeSkipInputField.text, out float Years))
        {
            return;
        }

        CDT += TimeSpan.FromDays(Years * 365);
    }
}
