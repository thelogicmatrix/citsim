﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class EntityEvents
{
    public DateTime ActivationTime;

    public TimeSpan Duration;

    public TimeSpan RescheduleInterval;
}
