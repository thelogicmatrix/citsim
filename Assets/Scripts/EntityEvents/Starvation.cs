﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Starvation : EntityEvents
{
    public bool IsStarving;

    public DateTime FullRecoveryDate;
}
