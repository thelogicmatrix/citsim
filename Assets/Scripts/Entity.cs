﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class Entity : MonoBehaviour
{
    public System.TimeSpan Age;

    public System.DateTime DOB, UDD, DCD; //Date of Birth, Ultimate Death Date, Death Check Date

    public float NetWorth; //Wealth Index will be implemented in the near future

    public List<EntityEvents> Events;

    public bool IsSimulating;

    public void Init()
    {
        IsSimulating = false;

        Events.Add(new Eat
        {
            ActivationTime = GameManager.CDT + System.TimeSpan.FromDays(1),

            Duration = System.TimeSpan.FromHours(1),

            RescheduleInterval = System.TimeSpan.FromHours(3)
        });

        Events.Add(new Work
        {
            ActivationTime = GameManager.CDT + System.TimeSpan.FromDays(0.25f),

            Duration = System.TimeSpan.FromHours(8),

            RescheduleInterval = System.TimeSpan.FromDays(1)
        });

        if (Age == default)
        {
            Age = System.TimeSpan.Zero;
        }

        DOB = GameManager.CDT - Age;

        DCD = GenDCD();

        UDD = DOB + GameManager.MLE;

        //print(DCD);

        StartCoroutine(Death());

        StartCoroutine(EventManager());
    }

    System.DateTime GenDCD()
    {
        float MaxLifeExpectancyInMins = (float)GameManager.MLE.TotalMinutes;

        var Offset = Random.Range((float)System.TimeSpan.FromMinutes(1).TotalDays, MaxLifeExpectancyInMins);
        
        return DOB + System.TimeSpan.FromMinutes(Offset);
    }

    public IEnumerator EventManager()
    {
        if (IsSimulating)
        {
            yield break;
        }

        IsSimulating = true;

        if (Events == default)
        {
            yield break;
        }

        var CDT = GameManager.CDT;

        System.DateTime LatestEventCompletionDate = default;

        Task.Run(async () =>
        {
            while (true)
            {
                int I = 0;

                foreach (EntityEvents FirstEvent in Events)
                {
                    if (FirstEvent.ActivationTime > CDT)
                    {
                        continue;
                    }

                    switch (FirstEvent)
                    {
                        case Eat _Eat:
                            {
                                if (NetWorth - 20 < 0) //If you can't afford food
                                {
                                    if (Events.Where(x => x is Starvation).FirstOrDefault() != default)
                                    {
                                        print("Has Starve Event");

                                        Starvation Starve = Events.Where(x => x is Starvation).First() as Starvation;

                                        Starve.IsStarving = true;
                                    }
                                    else
                                    {
                                        print("No Starve Event");

                                        Events.Add(new Starvation
                                        {
                                            ActivationTime = _Eat.ActivationTime + System.TimeSpan.FromDays(14)
                                        });
                                    }

                                    LatestEventCompletionDate = EventRescheduler(_Eat, Events);
                                }

                                else
                                {
                                    if (Events.Where(x => x is Starvation).FirstOrDefault() != default)
                                    {
                                        Starvation Starve = Events.Where(x => x is Starvation).First() as Starvation;

                                        if (Starve.FullRecoveryDate <= _Eat.ActivationTime)
                                        {
                                            Starve.FullRecoveryDate = System.DateTime.MinValue;
                                        }

                                        Starve.IsStarving = false;

                                        Starve.FullRecoveryDate = _Eat.ActivationTime + System.TimeSpan.FromDays(3);
                                    }

                                    NetWorth -= 20;
                                }

                                LatestEventCompletionDate = EventRescheduler(_Eat, Events);

                                break;
                            }
                        case Work _Work:
                            {
                                print("Work");

                                NetWorth += 0f;

                                if (Events.Where(x => x is Starvation).FirstOrDefault() != default)
                                {
                                    Starvation Starve = Events.Where(x => x is Starvation).First() as Starvation;

                                    if (Starve.IsStarving)
                                    {
                                        if (NetWorth - 20 >= 0)
                                        {
                                            print("Trigger Eat");

                                            Events.Where(x => x is Eat).First().ActivationTime = _Work.ActivationTime;
                                        }
                                    }
                                }

                                LatestEventCompletionDate = EventRescheduler(_Work, Events);

                                break;
                            }
                        case Starvation _Starve:
                            {
                                print("Starvation");

                                if (_Starve.FullRecoveryDate == System.DateTime.MinValue)
                                {
                                    continue;
                                }

                                if (!_Starve.IsStarving)
                                {
                                    continue;
                                }

                                Destroy(this.gameObject);

                                break;
                            }
                    }

                    I++;
                }

                if (I == 0)
                {
                    break;
                }
            }

            Events.RemoveAll(x => x is Starvation && x.ActivationTime == System.DateTime.MinValue);

            IsSimulating = false;
        });

        yield return new WaitForSeconds(1);

        StartCoroutine(EventManager());
    }

    public System.DateTime EventRescheduler(EntityEvents CurrentEvent, List<EntityEvents> Events)
    {
        var Y = Events.Where(x => x != CurrentEvent).OrderBy(x => x.ActivationTime).ToList();

        var LatestEventCompletionTime = CurrentEvent.ActivationTime + CurrentEvent.Duration;

        CurrentEvent.ActivationTime = default;

        foreach (EntityEvents E in Y)
        {
            if (E.ActivationTime > LatestEventCompletionTime + CurrentEvent.RescheduleInterval)
            {
                CurrentEvent.ActivationTime = LatestEventCompletionTime + CurrentEvent.RescheduleInterval;

                break;
            }
        }

        if (CurrentEvent.ActivationTime == default)
        {
            CurrentEvent.ActivationTime = Y.Last().ActivationTime + Y.Last().RescheduleInterval;
        }

        return LatestEventCompletionTime;
    }

    public IEnumerator Death()
    {
        Age = GameManager.CDT - DOB;

        //Should be prioritized since Entity can die off earlier than CDC and UDD

        if (!IsSimulating)
        {
            StartCoroutine(EventManager());
        }

        if (Age >= UDD - DOB)
        {
            //Death

            Destroy(this.gameObject);
        }

        if (Age >= DCD - DOB)
        {
            //Check for Death

            //For now we shall assume that Death is guaranteed once the Entity hits its DCD.

            Destroy(this.gameObject);

        }

        yield return new WaitForSeconds(1);

        StartCoroutine(Death());
    }
}
