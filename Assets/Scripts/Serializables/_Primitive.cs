﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class _Primitive : _Entity
{
    public float AgeInYears;
}
