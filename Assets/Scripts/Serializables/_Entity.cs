﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class _Entity
{
    public System.TimeSpan Age;

    public System.DateTime DOB, UDD, DCD; //Date of Birth, Ultimate Death Date, Death Check Date

    public float NetWorth; //Wealth Index will be implemented in the near future

    public List<EntityEvents> Events;
}
